# minecraft-ishima

Repository for the minecraft server Cacus.

## Server

[cacus.minecraft.waima.nu](minecraft://connect/cacus.minecraft.waima.nu)

## Mumble

[mumble.waima.nu](mumble://mumble.waima.nu)

## Map

[cacus.minecraft.waima.nu](https://cacus.minecraft.waima.nu/)

## Mods

| Name                                                      | Discreption                          |
|:----------------------------------------------------------|:-------------------------------------|
| Waystones                                                 | Teleport stones                      |
| [Balm](https://www.curseforge.com/minecraft/mc-mods/balm) | Library mod                          |
| Macaw's Bridges                                           | Bridges                              |
| Sophisticated Backpacks                                   | Backpacks                            |
| Clumps                                                    | Bundle XP Orbs                       |
| Silent Gear                                               | Tools                                |
| Silent Lib                                                | Library mod for Silent Gear          |
| Silent's Gems                                             | Gems for Silent Gear                 |
| ForgeEndertech                                            | Library mod for Advanced Chimneys    |
| Supplementaries                                           | Decoration stuff                     |
| Valhelsia Structures                                      | More Structures                      |
| Valhelsia Core                                            | Library mod for Valhelsia Structures |
| Filtered Chests                                           | CHest with filter                    |
| Tom's Simple Storage Mod                                  | Storage System                       |
| Awesome Dungeon - Forge                                   | More Dungeons                        |
| Library Ferret - Forge                                    | Library mod for Awesome Dungeon      |
| Storage Drawers                                           | Storage mod                          |
